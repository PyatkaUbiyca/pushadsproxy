// Code generated by easyjson for marshaling/unmarshaling. DO NOT EDIT.

package main

import (
	json "encoding/json"
	easyjson "github.com/mailru/easyjson"
	jlexer "github.com/mailru/easyjson/jlexer"
	jwriter "github.com/mailru/easyjson/jwriter"
)

// suppress unused package warning
var (
	_ *json.RawMessage
	_ *jlexer.Lexer
	_ *jwriter.Writer
	_ easyjson.Marshaler
)

func easyjson2726c826DecodePushadsproxy(in *jlexer.Lexer, out *DatsPushhResponse) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeString()
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "response":
			(out.Response).UnmarshalEasyJSON(in)
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson2726c826EncodePushadsproxy(out *jwriter.Writer, in DatsPushhResponse) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"response\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		(in.Response).MarshalEasyJSON(out)
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v DatsPushhResponse) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjson2726c826EncodePushadsproxy(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v DatsPushhResponse) MarshalEasyJSON(w *jwriter.Writer) {
	easyjson2726c826EncodePushadsproxy(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *DatsPushhResponse) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjson2726c826DecodePushadsproxy(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *DatsPushhResponse) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjson2726c826DecodePushadsproxy(l, v)
}
func easyjson2726c826DecodePushadsproxy1(in *jlexer.Lexer, out *DatsPushContent) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeString()
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "content":
			if in.IsNull() {
				in.Skip()
				out.Content = nil
			} else {
				in.Delim('[')
				if out.Content == nil {
					if !in.IsDelim(']') {
						out.Content = make([]DatsPushAd, 0, 1)
					} else {
						out.Content = []DatsPushAd{}
					}
				} else {
					out.Content = (out.Content)[:0]
				}
				for !in.IsDelim(']') {
					var v1 DatsPushAd
					(v1).UnmarshalEasyJSON(in)
					out.Content = append(out.Content, v1)
					in.WantComma()
				}
				in.Delim(']')
			}
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson2726c826EncodePushadsproxy1(out *jwriter.Writer, in DatsPushContent) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"content\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		if in.Content == nil && (out.Flags&jwriter.NilSliceAsEmpty) == 0 {
			out.RawString("null")
		} else {
			out.RawByte('[')
			for v2, v3 := range in.Content {
				if v2 > 0 {
					out.RawByte(',')
				}
				(v3).MarshalEasyJSON(out)
			}
			out.RawByte(']')
		}
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v DatsPushContent) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjson2726c826EncodePushadsproxy1(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v DatsPushContent) MarshalEasyJSON(w *jwriter.Writer) {
	easyjson2726c826EncodePushadsproxy1(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *DatsPushContent) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjson2726c826DecodePushadsproxy1(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *DatsPushContent) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjson2726c826DecodePushadsproxy1(l, v)
}
func easyjson2726c826DecodePushadsproxy2(in *jlexer.Lexer, out *DatsPushAd) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeString()
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "title":
			out.Title = string(in.String())
		case "content":
			out.Description = string(in.String())
		case "icon":
			out.Icon = string(in.String())
		case "image":
			out.Image = string(in.String())
		case "url":
			out.Link = string(in.String())
		case "price":
			out.Cpc = float64(in.Float64())
		case "Feed":
			out.Feed = int(in.Int())
		case "win_url":
			out.WinUrl = string(in.String())
		case "Buttons":
			if in.IsNull() {
				in.Skip()
				out.Buttons = nil
			} else {
				if out.Buttons == nil {
					out.Buttons = new([]Button)
				}
				if in.IsNull() {
					in.Skip()
					*out.Buttons = nil
				} else {
					in.Delim('[')
					if *out.Buttons == nil {
						if !in.IsDelim(']') {
							*out.Buttons = make([]Button, 0, 1)
						} else {
							*out.Buttons = []Button{}
						}
					} else {
						*out.Buttons = (*out.Buttons)[:0]
					}
					for !in.IsDelim(']') {
						var v4 Button
						easyjson2726c826DecodePushadsproxy3(in, &v4)
						*out.Buttons = append(*out.Buttons, v4)
						in.WantComma()
					}
					in.Delim(']')
				}
			}
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson2726c826EncodePushadsproxy2(out *jwriter.Writer, in DatsPushAd) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"title\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		out.String(string(in.Title))
	}
	{
		const prefix string = ",\"content\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		out.String(string(in.Description))
	}
	{
		const prefix string = ",\"icon\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		out.String(string(in.Icon))
	}
	{
		const prefix string = ",\"image\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		out.String(string(in.Image))
	}
	{
		const prefix string = ",\"url\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		out.String(string(in.Link))
	}
	{
		const prefix string = ",\"price\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		out.Float64(float64(in.Cpc))
	}
	{
		const prefix string = ",\"Feed\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		out.Int(int(in.Feed))
	}
	{
		const prefix string = ",\"win_url\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		out.String(string(in.WinUrl))
	}
	{
		const prefix string = ",\"Buttons\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		if in.Buttons == nil {
			out.RawString("null")
		} else {
			if *in.Buttons == nil && (out.Flags&jwriter.NilSliceAsEmpty) == 0 {
				out.RawString("null")
			} else {
				out.RawByte('[')
				for v5, v6 := range *in.Buttons {
					if v5 > 0 {
						out.RawByte(',')
					}
					easyjson2726c826EncodePushadsproxy3(out, v6)
				}
				out.RawByte(']')
			}
		}
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v DatsPushAd) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjson2726c826EncodePushadsproxy2(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v DatsPushAd) MarshalEasyJSON(w *jwriter.Writer) {
	easyjson2726c826EncodePushadsproxy2(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *DatsPushAd) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjson2726c826DecodePushadsproxy2(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *DatsPushAd) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjson2726c826DecodePushadsproxy2(l, v)
}
func easyjson2726c826DecodePushadsproxy3(in *jlexer.Lexer, out *Button) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeString()
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "action":
			out.Action = string(in.String())
		case "link":
			out.Link = string(in.String())
		case "title":
			out.Title = string(in.String())
		case "icon":
			out.Icon = string(in.String())
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson2726c826EncodePushadsproxy3(out *jwriter.Writer, in Button) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"action\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		out.String(string(in.Action))
	}
	{
		const prefix string = ",\"link\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		out.String(string(in.Link))
	}
	{
		const prefix string = ",\"title\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		out.String(string(in.Title))
	}
	{
		const prefix string = ",\"icon\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		out.String(string(in.Icon))
	}
	out.RawByte('}')
}
