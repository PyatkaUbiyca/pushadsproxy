package main

import (
	"github.com/qiangxue/fasthttp-routing"
	"net/url"
	"pushadsproxy/helpers"
	"pushadsproxy/ip"
	"strings"
)

type DatsPush struct {
	Url      string
	Ctx      *routing.Context
	Response *MegaPushAd
}

//easyjson:json
type DatsPushhResponse struct {
	Response DatsPushContent `json:"response"`
}

//easyjson:json
type DatsPushContent struct {
	Content []DatsPushAd `json:"content"`
}

//easyjson:json
type DatsPushAd struct {
	Title       string  `json:"title"`
	Description string  `json:"content"`
	Icon        string  `json:"icon"`
	Image       string  `json:"image"`
	Link        string  `json:"url"`
	Cpc         float64 `json:"price"`
	Feed        int
	WinUrl      string `json:"win_url"`
	Buttons     *[]Button
}

func NewDatsPush(ctx *routing.Context) *DatsPush {

	m := new(DatsPush)
	m.Ctx = ctx
	m.Url = "http://dsp-eu.datspush.com/v1/external/native/G0wrsE3qzUWH8ssPeLvF?user_ip={ip}&user_agent={ua}&source_id=1&subscriber_id={subscriber_id}"

	ua := string(ctx.UserAgent())
	ip := ip.GetIPAdress(ctx)

	params := Params{
		Ua:           ua,
		Ip:           ip,
		SubscriberId: helpers.GetMD5Hash(ua + ip),
	}
	m.replacePlaceHolders(params)

	return m
}

func (m *DatsPush) replacePlaceHolders(p Params) {
	t := &url.URL{Path: p.Ua}
	ua := t.String()

	r := strings.NewReplacer(
		"{ip}", p.Ip,
		"{ua}", ua,
		"{subscriber_id}", p.SubscriberId,
	)

	m.Url = r.Replace(m.Url)

}

func (m *DatsPush) GetAd() []Response {

	body := getFeed(m.Url)
	var r []Response
	if body != nil && len(body) > 2 {
		ad := DatsPushhResponse{}
		ad.UnmarshalJSON(body)

		for _, ad := range ad.Response.Content {
			ad.Feed = DATS_PUSH_ID
			r = append(r, Response(ad))
		}
	}

	return r

}
