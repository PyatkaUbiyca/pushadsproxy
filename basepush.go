package main

import (
	"github.com/qiangxue/fasthttp-routing"
	"net/url"
	"pushadsproxy/helpers"
	"pushadsproxy/ip"
	"strings"
)

type BasePushPush struct {
	Url      string
	Ctx      *routing.Context
	Response *MegaPushAd
}

//easyjson:json
type BasePushPushAd struct {
	Title       string  `json:"title"`
	Description string  `json:"description"`
	Icon        string  `json:"icon"`
	Image       string  `json:"image"`
	Link        string  `json:"url"`
	Cpc         float64 `json:"price"`
	Feed        int
	WinUrl      string `json:"-"`
	Buttons     *[]Button
}

func NewBasePushPush(ctx *routing.Context) *BasePushPush {

	m := new(BasePushPush)
	m.Ctx = ctx
	m.Url = "http://pushup.space/feed/get?ua={ua}&userId={subscriber_id}&ip={ip}&country={country}&key=b2f33f1a-5535-4c37-9931-43a8ba45ef2"

	ip := ip.GetIPAdress(ctx)
	ua := string(ctx.UserAgent())
	params := Params{
		Ua:           	ua,
		Ip:           	ip,
		CountryCode:	ctx.Get("Country").(string),
		SubscriberId: 	helpers.GetMD5Hash(ua + ip),
	}
	m.replacePlaceHolders(params)

	return m
}

func (m *BasePushPush) replacePlaceHolders(p Params) {
	t := &url.URL{Path: p.Ua}
	ua := t.String()

	r := strings.NewReplacer(
		"{ip}", p.Ip,
		"{ua}", ua,
		"{country}", p.CountryCode,
		"{subscriber_id}", p.SubscriberId,
	)

	m.Url = r.Replace(m.Url)
}

func (m *BasePushPush) GetAd() []Response {
	body := getFeed(m.Url)
	var r []Response
	if body != nil && len(body) > 2 {
		ad := BasePushPushAd{}
		ad.UnmarshalJSON(body)
		ad.Feed = BASE_PUSH_ID

		r = append(r,Response(ad))
	}

	return r
}
