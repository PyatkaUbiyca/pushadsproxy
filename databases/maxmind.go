package databases

import (
	"github.com/oschwald/maxminddb-golang"
	"log"
	"net"
)

var GeoDb *maxminddb.Reader

type onlyCountry struct {
	Country struct {
		ISOCode string `maxminddb:"iso_code"`
	} `maxminddb:"country"`
}

func GeoDbInit(Path string) *maxminddb.Reader {
	geo, err := maxminddb.Open(Path)

	if err != nil {
		log.Fatalf("error when maxmind DB loaded: %s", err)
	}
	return geo
}

func GetCountry(ip string) string {

	var record onlyCountry
	var err error

	i := net.ParseIP(ip)
	err = GeoDb.Lookup(i, &record)
	if err != nil {
		log.Fatalf("maxmind db is not initialize: %s", err)
	}

	if record.Country.ISOCode == "" {
		record.Country.ISOCode = "XX"
	}

	return record.Country.ISOCode
}
