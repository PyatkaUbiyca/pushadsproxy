package databases

import (
	"database/sql"
	"fmt"
	"github.com/kshvakov/clickhouse"
)

var ClickHouse *sql.DB

func ConnectClickHouseInit(Path string) (*sql.DB, error) {

	connect, err := sql.Open("clickhouse", Path)
	if err != nil {
		return nil, err
	}
	if err := connect.Ping(); err != nil {
		if exception, ok := err.(*clickhouse.Exception); ok {
			fmt.Printf("[%d] %s \n%s\n", exception.Code, exception.Message, exception.StackTrace)
		} else {
			return nil, err
		}
	}

	return connect, nil

}
