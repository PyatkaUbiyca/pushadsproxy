package main

import (
	"github.com/qiangxue/fasthttp-routing"
	"github.com/valyala/fasthttp"
	"pushadsproxy/helpers"
	"pushadsproxy/ip"
	"strings"
)

type MarketGid struct {
	Url      string
	Ctx      *routing.Context
	Response *MegaPushAd
}

//easyjson:json
type MarketGidAd struct {
	Title       string  `json:"title"`
	Description string  `json:"description"`
	Icon        string  `json:"icon"`
	Image       string  `json:"image"`
	Link        string  `json:"link"`
	Cpc         float64 `json:"cpc"`
	Feed        int
	WinUrl      string `json:"win_url"`
	Buttons     *[]Button
}

func NewMarketGid(ctx *routing.Context) *MarketGid {

	m := new(MarketGid)
	m.Ctx = ctx
	m.Url = "http://api.traffic-media.co/677321?content_type=json&token=833c9c54b50616d4149a27da125d7d30&ip={ip}&ua={ua}&muid={subscriber_id}"

	user_ip := ip.GetIPAdress(ctx)
	ua := string(ctx.UserAgent())

	params := Params{
		Ua:           ua,
		Ip:           user_ip,
		SubscriberId: helpers.GetMD5Hash(ua + user_ip),
	}
	m.replacePlaceHolders(params)

	return m
}

func (m *MarketGid) replacePlaceHolders(p Params) {

	r := strings.NewReplacer(
		"{ip}", p.Ip,
		"{ua}", p.Ua,
		"{subscriber_id}", p.SubscriberId,
	)

	m.Url = r.Replace(m.Url)

}

func (m *MarketGid) GetAd() Response {

	var body []byte
	acceptLanguageHeader := "Accept-Language"

	req := fasthttp.AcquireRequest()
	req.SetRequestURI(m.Url)
	acceptLanguage := m.Ctx.Request.Header.Peek(acceptLanguageHeader)

	req.Header.Add(acceptLanguageHeader, string(acceptLanguage))

	resp := fasthttp.AcquireResponse()

	fasthttp.Do(req,resp)
	if resp.StatusCode() == 200 {
		body = resp.Body()
	} else {
		body = nil
	}
	fasthttp.ReleaseRequest(req)
	fasthttp.ReleaseResponse(resp)

	var r Response
	if body != nil && len(body) > 2 {
		body = body[1 : len(body)-1]
		ad := MarketGidAd{}
		ad.UnmarshalJSON(body)
		ad.Feed = MARKETGID_ID
		r = Response(ad)
	}

	return r
}
