package helpers

import (
	"log"
	"net"
	"pushadsproxy/countrycodes"
	"pushadsproxy/databases"
)

type onlyCountry struct {
	Country struct {
		ISOCode string `maxminddb:"iso_code"`
	} `maxminddb:"country"`
}

func GetCountry(ip string) string {
	var record onlyCountry

	i := net.ParseIP(ip)
	err := databases.GeoDb.Lookup(i, &record)
	if err != nil {
		log.Printf("error when maxmind lockup: %s", err)
	}

	if record.Country.ISOCode == "" {
		record.Country.ISOCode = "US"
	}

	country, _ := countrycodes.GetByAlpha2(record.Country.ISOCode)

	return country.Alpha3
}
