package helpers

import (
	"crypto/md5"
	"encoding/hex"
	"github.com/satori/go.uuid"
)

func GetMD5Hash(text string) string {
	hash := md5.Sum([]byte(text))
	return hex.EncodeToString(hash[:])
}

func GenUUIDv4() []byte {
	id, _ := uuid.NewV4()
	return id.Bytes()
}
