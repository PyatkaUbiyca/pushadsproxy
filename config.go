package main

var Config = struct {
	Port string `required:"true"`

	MaxMind struct {
		Path string `required:"true"`
	}

	ClickHouse struct {
		Dsn string `required:"true"`
	}

	Stats struct {
		FeedCounter int `required:"true"`
	}
}{}
