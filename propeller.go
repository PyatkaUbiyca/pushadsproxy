package main

import (
	"github.com/qiangxue/fasthttp-routing"
	"net/url"
	"pushadsproxy/ip"
	"strings"
)

type PropellerPush struct {
	Url      string
	Ctx      *routing.Context
	Response *MegaPushAd
}

//easyjson:json
type PropellerPushAds struct {
	Ads []PropellerPushAd `json:"ads"`
}

//easyjson:json
type PropellerPushAd struct {
	Title       string  `json:"title"`
	Description string  `json:"text"`
	Icon        string  `json:"icon"`
	Image       string  `json:"image"`
	Link        string  `json:"click_url"`
	Cpc         float64 `json:"cpc_rate"`
	Feed        int
	WinUrl      string `json:"win_url"`
	Buttons     *[]Button
}

func NewPropellerPush(ctx *routing.Context) *PropellerPush {

	m := new(PropellerPush)
	m.Ctx = ctx
	m.Url = "https://offers.propellerads.com/api/v1/ads/2193519/?auth=f43cfdd5fbbe1d5c1465da05ec66bc3fa064c7a6&ua={ua}&ip={ip}&var=1"

	params := Params{
		Ua: string(ctx.UserAgent()),
		Ip: ip.GetIPAdress(ctx),
	}
	m.replacePlaceHolders(params)

	return m
}

func (m *PropellerPush) replacePlaceHolders(p Params) {
	t := &url.URL{Path: p.Ua}
	ua := t.String()

	r := strings.NewReplacer(
		"{ip}", p.Ip,
		"{ua}", ua,
	)

	m.Url = r.Replace(m.Url)

}

func (m *PropellerPush) GetAd() []Response {

	body := getFeed(m.Url)
	var r []Response
	if body != nil && len(body) > 2 {
		ad := PropellerPushAds{}
		ad.UnmarshalJSON(body)

		for _, ad := range ad.Ads {
			ad.Feed = PROPELLER_ID
			r = append(r, Response(ad))
		}
	}

	return r
}
