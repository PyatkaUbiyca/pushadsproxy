package main

const (
	MEGAPUSH_ID  = 10
	MARKETGID_ID = 20
	DATS_PUSH_ID = 30
	RICH_PUSH_ID = 40
	EVA_DAV_ID   = 50
	PROPELLER_ID = 60
	FUJITSU_ID   = 70
	ADSKEEPER    = 80
	MAVEN        = 90
	TRFEED       = 100
	CLIKAINE_ID     = 110
	BASE_PUSH_ID = 120
)

type Feed interface {
	GetAd() []Response
	replacePlaceHolders(Params)
}

type Params struct {
	Ua           string
	Ip           string
	Lang         string
	CountryCode  string
	SubscriberId string
}

type Button struct {
	Action 	string `json:"action"`
	Link 	string `json:"link"`
	Title 	string `json:"title"`
	Icon 	string `json:"icon"`
}

type Response struct {
	Title       string  	`json:"title"`
	Description string  	`json:"description"`
	Icon        string  	`json:"icon"`
	Image       string  	`json:"image"`
	Link        string  	`json:"link"`
	Cpc         float64 	`json:"cpc"`
	Feed        int     	`json:"feed_id"`
	WinUrl      string 		`json:"winurl"`
	Buttons     *[]Button 	`json:"buttons"`
}

func (r *Response) AddCancelButtons() {

	r.Buttons = &[]Button{
		Button{
				Link: r.Link,
				Action: "1",
				Title: "Cancel",
			},
		Button{
			Link: r.Link,
			Action: "2",
			Title: "Unsubscribe",
		},
	}

}


func getFeed(url string) []byte {
	var dst []byte

	statusCode, body, err := HttpClient.Get(dst, url)

	if err != nil {
		//fmt.Println(err)
	//	fmt.Println(url)
	}

//	fmt.Println(url)
//	fmt.Println(statusCode)
	if statusCode == 200 {
		return body
	} else {
		return nil
	}
}
