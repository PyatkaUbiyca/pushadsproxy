package main

import (
	"github.com/qiangxue/fasthttp-routing"
	"net/url"
	"pushadsproxy/ip"
	"strings"
	"time"
)

type MegaPush2 struct {
	Url      string
	Ctx      *routing.Context
	Response *MegaPushAd
}

//easyjson:json
type MegaPushAds2 []MegaPushAd

//easyjson:json
type MegaPushAd2 struct {
	Title       string  `json:"title"`
	Description string  `json:"description"`
	Icon        string  `json:"icon"`
	Image       string  `json:"image"`
	Link        string  `json:"link"`
	Cpc         float64 `json:"cpc"`
	Feed        int
	WinUrl      string `json:"win_url"`
	Buttons     *[]Button
}

func NewMegaPush2(ctx *routing.Context) *MegaPush {

	m := new(MegaPush)
	m.Ctx = ctx
	m.Url = "http://951721.extemedia.com/code/feed/?pid=951721&ip={ip}&ua={ua}&lang={lang}&limit=10"

	params := Params{
		Ua:   string(ctx.UserAgent()),
		Lang: string(ctx.Request.Header.Peek("Accept-Language")),
		Ip:   ip.GetIPAdress(ctx),
	}
	m.replacePlaceHolders(params)

	return m
}

func (m *MegaPush2) replacePlaceHolders(p Params) {
	t := &url.URL{Path: p.Ua}
	ua := t.String()

	r := strings.NewReplacer(
		"{ip}", p.Ip,
		"{ua}", ua,
		"{lang}", p.Lang,
	)

	m.Url = r.Replace(m.Url)

}

func (m *MegaPush2) GetAd() []Response {

	currentHour := time.Now().Hour()

	if currentHour <= 8 && currentHour >= 1 {
		return make([]Response,0)
	}

	body := getFeed(m.Url)
	var r []Response
	if body != nil && len(body) > 2 {
		//body = body[1 : len(body)-1]
		ads := MegaPushAds{}
		ads.UnmarshalJSON(body)

		for _, ad := range ads {
			ad.Feed = MEGAPUSH_ID

			resp := Response(ad)
			resp.AddCancelButtons()
			r = append(r, resp)
		}
	}

	return r
}
