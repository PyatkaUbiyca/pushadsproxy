package main

import (
	"github.com/qiangxue/fasthttp-routing"
	"net/url"
	"pushadsproxy/helpers"
	"pushadsproxy/ip"
	"strings"
)

type EvaDav struct {
	Url      string
	Ctx      *routing.Context
	Response *MegaPushAd
}

//easyjson:json
type EvaDavAd struct {
	Title       string  `json:"title"`
	Description string  `json:"descr"`
	Icon        string  `json:"icon"`
	Image       string  `json:"image"`
	Link        string  `json:"link"`
	Cpc         float64 `json:"cpc"`
	Feed        int
	WinUrl      string `json:"win_url"`
	Buttons     *[]Button
}

func NewEvaDav(ctx *routing.Context) *EvaDav {

	m := new(EvaDav)
	m.Ctx = ctx
	m.Url = "http://eu1.evadavdsp.pro/dsp/ph/feed?sspid=143&secret=oSwsGsFkecWipu2jHvzy&ip={ip}&ua={ua}&subid=1&uid={subscriber_id}&accept_lang={lang}"

	ip := ip.GetIPAdress(ctx)
	ua := string(ctx.UserAgent())

	params := Params{
		Ua:           ua,
		Ip:           ip,
		Lang:         string(ctx.Request.Header.Peek("Accept-Language")),
		SubscriberId: helpers.GetMD5Hash(ua + ip),
	}
	m.replacePlaceHolders(params)

	return m
}

func (m *EvaDav) replacePlaceHolders(p Params) {
	t := &url.URL{Path: p.Ua}
	ua := t.String()

	r := strings.NewReplacer(
		"{ip}", p.Ip,
		"{ua}", ua,
		"{subscriber_id}", p.SubscriberId,
		"{lang}", p.Lang,
	)

	m.Url = r.Replace(m.Url)

}

func (m *EvaDav) GetAd() Response {

	body := getFeed(m.Url)
	var r Response
	if body != nil && len(body) > 2 {
		ad := EvaDavAd{}
		ad.UnmarshalJSON(body)
		ad.Feed = EVA_DAV_ID
		r = Response(ad)
	}

	return r
}
