package main

import (
	"encoding/xml"
	"github.com/qiangxue/fasthttp-routing"
	"net/url"
	"pushadsproxy/ip"
	"strings"
)

type RichPush struct {
	Url      string
	Ctx      *routing.Context
	Request  string
	Response *MegaPushAd
}

type RichPushAds struct {
	Ads    []RichPushAd `xml:"teaser"`
	Price  float64      `xml:"price"`
	WinUrl string       `xml:"img_stat_url"`
}

type RichPushAd struct {
	Title       string  `xml:"lb_title"`
	Description string  `xml:"sub_title"`
	Icon        string  `xml:"lb_img_url"`
	Image       string  `xml:"lb_rich_img_url"`
	Link        string  `xml:"lb_click_url"`
	Cpc         float64 `xml:"cpc_rate"`
	Feed        int
	WinUrl      string
	Buttons     *[]Button
}

func NewRichPush(ctx *routing.Context) *RichPush {

	m := new(RichPush)
	m.Ctx = ctx
	m.Url = "http://n.ads3-adnow.com/a?Id=600701&out=xml&d_ip={ip}&node=ADNOW_affi&d_user_agent={ua}"

	ua := string(ctx.UserAgent())
	ip := ip.GetIPAdress(ctx)

	params := Params{
		Ua: ua,
		Ip: ip,
	}
	m.replacePlaceHolders(params)

	return m
}

func (m *RichPush) replacePlaceHolders(p Params) {
	t := &url.URL{Path: p.Ua}
	ua := t.String()

	r := strings.NewReplacer(
		"{ip}", p.Ip,
		"{ua}", ua,
		"{subscriber_id}", p.SubscriberId,
		"{country}", p.CountryCode,
	)

	m.Url = r.Replace(m.Url)

}

func (m *RichPush) GetAd() []Response {
	body := getFeed(m.Url)
	var r []Response
	if body != nil && !strings.Contains(string(body), "No win resp") {
		ad := RichPushAds{}
		xml.Unmarshal(body, &ad)

		if len(ad.Ads) == 0 {
			println(string(body))
		}

		for _, ad_item := range ad.Ads {
			ad_item.Cpc = ad.Price
			ad_item.WinUrl = ad.WinUrl
			ad_item.Feed = RICH_PUSH_ID
			r = append(r, Response(ad_item))
		}
	}

	return r
}
