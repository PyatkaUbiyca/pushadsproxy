package main

import (
	"github.com/qiangxue/fasthttp-routing"
	"pushadsproxy/ip"
	"strings"
)

type Clickaine struct {
	Url      string
	Ctx      *routing.Context
	Response *CLickaineAd
}

//easyjson:json
type CLickaineAd struct {
	Title       string  `json:"title"`
	Description string  `json:"body"`
	Icon        string  `json:"icon"`
	Image       string  `json:"image"`
	Link        string  `json:"link"`
	Cpc         float64 `json:"cpc"`
	Feed        int
	WinUrl      string
	Buttons     *[]Button
}

func NewCLickaine(ctx *routing.Context) *Clickaine {

	m := new(Clickaine)
	m.Ctx = ctx
	m.Url = "https://ca.clcknads.pro/v2/rtb/exchange/a9ea161f96e0cccd9088bbb958732a2cb3dab2b024986439fddd713a70bb1019d24c1939956f062cc901fe11af023f57eaa2e8c8fedf2a39f64d0157ac3b87cf?format=push&ua={ua}&language={lang}&ip={ip}"

	user_ip := ip.GetIPAdress(ctx)
	ua := string(ctx.UserAgent())

	params := Params{
		Ua:         ua,
		Ip:         user_ip,
		Lang: 		string(m.Ctx.Request.Header.Peek("Accept-Language")),
	}
	m.replacePlaceHolders(params)

	return m
}

func (m *Clickaine) replacePlaceHolders(p Params) {

	r := strings.NewReplacer(
		"{ip}", p.Ip,
		"{ua}", p.Ua,
		"{lang}", p.Lang,
	)

	m.Url = r.Replace(m.Url)

}

func (m *Clickaine) GetAd() Response {

	body := getFeed(m.Url)
	var r Response

	if body != nil && len(body) > 2 {
		body = body[1 : len(body)-1]
		ad := CLickaineAd{}
		ad.UnmarshalJSON(body)
		ad.Feed = CLIKAINE_ID
		r = Response(ad)
	}

	return r
}
