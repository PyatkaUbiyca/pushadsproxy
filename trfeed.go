package main

import (
	"github.com/qiangxue/fasthttp-routing"
)

type TrFeed struct {
	Url      string
	Ctx      *routing.Context
	Response *MegaPushAd
}

func NewTrFeed(ctx *routing.Context) *TrFeed {

	m := new(TrFeed)
	m.Ctx = ctx

	return m
}

func (m *TrFeed) replacePlaceHolders(p Params) {

}

func (m *TrFeed) GetAd() []Response {

	r := []Response{
		Response{Image: "https://diablopush.com/images/image.png",
			Icon:        "https://diablopush.com/images/icon.png",
			Cpc:         1,
			Link:        "https://johnnytrack.me/click.php?key=w1dz3w3104jtlt2l0bh6",
			Title:       "Şans çarkını döndür! 🎡💰",
			Description: "🍀Casavole’e katıl, oynamaya hemen başla",
			Feed:        TRFEED,
		},
	}

	country := m.Ctx.Get("Country").(string)

	if country == "TR" {
		return r
	} else {
		return []Response{}
	}
}
