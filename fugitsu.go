package main

import (
	"github.com/qiangxue/fasthttp-routing"
	"net/url"
	"pushadsproxy/helpers"
	"pushadsproxy/ip"
	"strings"
)

type FugitsuPush struct {
	Url      string
	Ctx      *routing.Context
	Response *MegaPushAd
}

//easyjson:json
type FugitsuPushAd struct {
	Title       string  `json:"title"`
	Description string  `json:"desc"`
	Icon        string  `json:"icon_url"`
	Image       string  `json:"image_url"`
	Link        string  `json:"click_url"`
	Cpc         float64 `json:"cpc"`
	Feed        int
	WinUrl      string `json:"win_url"`
	Buttons     *[]Button
}

func NewFugitsuPush(ctx *routing.Context) *FugitsuPush {

	m := new(FugitsuPush)
	m.Ctx = ctx
	m.Url = "http://eu.binder.adplatform.pro/?token=E1Y8lqzLTgDpmcdyiabB4hYDgkgITFzwutk2eoLSax0NmsTvKZF1NGnBk1lPwZHp&lang={lang}&subid={subscriber_id}&ip={ip}&ua={ua}&sid=1"

	ip := ip.GetIPAdress(ctx)
	ua := string(ctx.UserAgent())
	params := Params{
		Ua:           ua,
		Ip:           ip,
		Lang:         string(ctx.Request.Header.Peek("Accept-Language")),
		SubscriberId: helpers.GetMD5Hash(ua + ip),
	}
	m.replacePlaceHolders(params)

	return m
}

func (m *FugitsuPush) replacePlaceHolders(p Params) {
	t := &url.URL{Path: p.Ua}
	ua := t.String()

	r := strings.NewReplacer(
		"{ip}", p.Ip,
		"{ua}", ua,
		"{lang}", p.Lang,
		"{subscriber_id}", p.SubscriberId,
	)

	m.Url = r.Replace(m.Url)
}

func (m *FugitsuPush) GetAd() []Response {
	body := getFeed(m.Url)
	var r []Response
	if body != nil && len(body) > 2 {
		ad := FugitsuPushAd{}
		ad.UnmarshalJSON(body)
		ad.Feed = FUJITSU_ID

		r = append(r,Response(ad))
	}

	return r
}
