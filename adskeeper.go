package main

import (
	"github.com/qiangxue/fasthttp-routing"
	"github.com/valyala/fasthttp"
	"net/url"
	"pushadsproxy/helpers"
	"pushadsproxy/ip"
	"strings"
)

type AdsKeeper struct {
	Url      string
	Ctx      *routing.Context
	Response *MegaPushAd
}

//easyjson:json
type AdsKeeperAds []AdsKeeperAd

//easyjson:json
type AdsKeeperAd struct {
	Title       string  `json:"title"`
	Description string  `json:"description"`
	Icon        string  `json:"icon"`
	Image       string  `json:"image"`
	Link        string  `json:"link"`
	Cpc         float64 `json:"cpc"`
	Feed        int
	WinUrl      string `json:"win_url"`
	Buttons     *[]Button
}

func NewAdsKeeper(ctx *routing.Context) *AdsKeeper {

	m := new(AdsKeeper)
	m.Ctx = ctx
	m.Url = "http://api.adskeeper.co.uk/310633?content_type=json&token=1c0f342f4476b392d82fd242ac023466&ip={ip}&ua={ua}&muid={subscriber_id}"

	ip := ip.GetIPAdress(ctx)
	ua := string(ctx.UserAgent())

	params := Params{
		Ua:           ua,
		Ip:           ip,
		SubscriberId: helpers.GetMD5Hash(ua + ip),
	}
	m.replacePlaceHolders(params)

	return m
}

func (m *AdsKeeper) replacePlaceHolders(p Params) {
	t := &url.URL{Path: p.Ua}
	ua := t.String()

	r := strings.NewReplacer(
		"{ip}", p.Ip,
		"{ua}", ua,
		"{subscriber_id}", p.SubscriberId,
	)

	m.Url = r.Replace(m.Url)

}

func (m *AdsKeeper) GetAd() []Response {

	var body []byte
	acceptLanguageHeader := "Accept-Language"

	req := fasthttp.AcquireRequest()
	req.SetRequestURI(m.Url)
	acceptLanguage := m.Ctx.Request.Header.Peek(acceptLanguageHeader)

	req.Header.Add(acceptLanguageHeader, string(acceptLanguage))

	resp := fasthttp.AcquireResponse()

	HttpClient.Do(req,resp)

	if resp.StatusCode() == 200 {
		body = resp.Body()
	} else {
		body = nil
	}
	fasthttp.ReleaseRequest(req)
	fasthttp.ReleaseResponse(resp)

	var r []Response
	if body != nil && len(body) > 2 {

		ads := AdsKeeperAds{}
		ads.UnmarshalJSON(body)
		for _, ad := range ads {
			ad.Feed = ADSKEEPER
			respose := Response(ad)
			respose.AddCancelButtons()
			r = append(r, respose)
		}
	}

	return r
}
