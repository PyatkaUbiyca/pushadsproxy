package models

import (
	"sync"
)

type CounterIntf interface {
	Add(data interface{})
	save()
}

type Counter struct {
	FlushInt int
	Len      int
	mu       sync.Mutex
}

func (c *Counter) add(child CounterIntf) {

	if c.Len >= c.FlushInt {
		go child.save()
		c.Len = 0
	}
}
