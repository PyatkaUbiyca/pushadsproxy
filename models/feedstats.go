package models

import (
	"github.com/kshvakov/clickhouse"
	"log"
	"pushadsproxy/databases"

	"time"
)

type FeedStatsCounter struct {
	Counter
	InsertObjects []FeedStats
}

func NewFeedStatCounter(flush int) *FeedStatsCounter {
	counter := new(FeedStatsCounter)
	counter.FlushInt = flush
	return counter
}

func (c *FeedStatsCounter) Add(data interface{}) {
	c.mu.Lock()
	c.InsertObjects = append(c.InsertObjects, data.(FeedStats))
	c.Len++
	c.add(c)
	c.mu.Unlock()
}

/*
CREATE TABLE stats.feed_views
(
    country FixedString(2),
    cpc Float64,
    feed_id Int8,
    impression_id FixedString(16),
	is_win Int8,
    action_day Date,
    action_time DateTime
)
ENGINE = MergeTree(action_day, (action_day, country, impression_id), 8192)
*/

func (fc *FeedStatsCounter) save() {

	ch := databases.ClickHouse

	var (
		tx, _   = ch.Begin()
		stmt, _ = tx.Prepare("INSERT INTO feed_views  (country, cpc, feed_id, impression_id, is_win, action_day, action_time) VALUES (?, ?, ?, ?, ?, ?, ?)")
	)

	for _, feedCount := range fc.InsertObjects {
		if _, err := stmt.Exec(
			feedCount.Country,
			feedCount.Cpc,
			feedCount.FeedId,
			feedCount.ImpressionId,
			feedCount.IsWin,
			clickhouse.Date(time.Now()),
			time.Now(),
		); err != nil {
			log.Println("error exec stmt: %s", err)
		}
	}
	if err := tx.Commit(); err != nil {
		log.Println("error save stats: %s", err)
	}
	fc.InsertObjects = nil
}
