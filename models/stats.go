package models

import (
	"time"
)

var ScopeFeedStats *FeedStatsCounter

type FeedStats struct {
	Country      string
	Cpc          float64
	FeedId       int
	ImpressionId []byte
	IsWin        int
	Time         time.Time
}

func CountFeed(country string, cpc float64, feedId int, impressionId []byte, isWin int) {

	fs := FeedStats{
		Country:      country,
		Cpc:          cpc,
		FeedId:       feedId,
		ImpressionId: impressionId,
		IsWin:        isWin,
		Time:         time.Now().UTC(),
	}

	ScopeFeedStats.Add(fs)
}
