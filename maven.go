package main

import (
	"github.com/qiangxue/fasthttp-routing"
	"net/url"
	"pushadsproxy/ip"
	"strings"
)

type MavenPush struct {
	Url      string
	Ctx      *routing.Context
	Response *MegaPushAd
}

//easyjson:json
type MavenListing struct {
	Listing MavenPushAds `json:"result"`
}

//easyjson:json
type MavenPushAds struct {
	Ads []MavenPushAd `json:"listing"`
}

//easyjson:json
type MavenPushAd struct {
	Title       string  `json:"title"`
	Description string  `json:"descr"`
	Icon        string  `json:"-"`
	Image       string  `json:"image"`
	Link        string  `json:"url"`
	Cpc         float64 `json:"bid"`
	Feed        int
	WinUrl      string 	`json:"pixel"`
	Buttons     *[]Button
}

func NewMavenPush(ctx *routing.Context) *MavenPush {

	m := new(MavenPush)
	m.Ctx = ctx
	m.Url = "http://xml.ad-maven.com/search?feed=164521&auth=ExpghL&subid=1&ua={ua}&url=http://comeads.com&user_ip={ip}&format=json&query=ron"

	params := Params{
		Ua: string(ctx.UserAgent()),
		Ip: ip.GetIPAdress(ctx),
	}
	m.replacePlaceHolders(params)

	return m
}

func (m *MavenPush) replacePlaceHolders(p Params) {
	t := &url.URL{Path: p.Ua}
	ua := t.String()

	r := strings.NewReplacer(
		"{ip}", p.Ip,
		"{ua}", ua,
	)

	m.Url = r.Replace(m.Url)

}

func (m *MavenPush) GetAd() Response {

	body := getFeed(m.Url)
	var r Response
	if body != nil && len(body) > 2 {
		ad := MavenListing{}
		ad.UnmarshalJSON(body)

		if len(ad.Listing.Ads) > 0 {
			adContent := ad.Listing.Ads[0]

			adContent.Feed = MAVEN
			adContent.Icon = adContent.Image
			r = Response(adContent)
		}
	}

	return r
}
