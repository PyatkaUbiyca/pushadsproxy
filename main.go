package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/jinzhu/configor"
	"github.com/qiangxue/fasthttp-routing"
	"github.com/valyala/fasthttp"
	"log"
	"pushadsproxy/databases"
	"pushadsproxy/helpers"
	"pushadsproxy/ip"
	"pushadsproxy/models"
	"sort"
	"sync"
	"time"
)

var (
	strContentType     = []byte("Content-Type")
	strApplicationJSON = []byte("application/json")
	strApplicationHTML = []byte("text/html; charset=utf-8")
	certPath = "/var/www/golang/src/cert/"
)

var HttpClient fasthttp.Client


func getAdHandler(ctx *routing.Context) {
	var feeds []Response

	if (ctx.Get("Country").(string) == "IN") {
		 feeds = runFeeds(
		 	NewPropellerPush(ctx),
		 //	NewBasePushPush(ctx),
			NewMegaPush(ctx))
		//	NewMegaPush2(ctx))
	} else {
		feeds = runFeeds(
			NewPropellerPush(ctx),
			//NewDatsPush(ctx),
			NewMegaPush(ctx),
			//NewMegaPush2(ctx),
			NewFugitsuPush(ctx),
			NewAdsKeeper(ctx),
			NewTrFeed(ctx),
			NewBasePushPush(ctx))
	}

	//feeds := runFeeds(NewBasePushPush(ctx))

	// Сортируем обьекты по CPC
	if len(feeds) > 0 {
		sort.Slice(feeds, func(i, j int) bool {
		//	feeds[i].AddCancelButtons()
			return feeds[i].Cpc > feeds[j].Cpc
		})
		saveStat(ctx, feeds[0:1])

	} else {
		feeds = append(feeds, Response{})
		saveStat(ctx, feeds)
	}


	/* TODO: вынести в action принимающий показ или вызывать из SW
	if len(r.WinUrl) > 0 {
		var dst []byte
		go fasthttp.Get(dst, r.WinUrl)
	}
	*/

	doJSONWrite(ctx, feeds)
}



func saveStat(ctx *routing.Context, feedRedults []Response) {
	var isWin int
	for i, result := range feedRedults {
		if i == 0 {
			isWin = 1
		} else {
			isWin = 0
		}
		if result.Title != "" {
			models.CountFeed(ctx.Get("Country").(string), result.Cpc, result.Feed, ctx.Get("RequestId").([]byte), isWin)
		} else {
			models.CountFeed(ctx.Get("Country").(string), 0, 0, ctx.Get("RequestId").([]byte), 0)
		}
	}
}

func doJSONWrite(ctx *routing.Context, obj []Response) {
	ctx.Response.Header.SetCanonical(strContentType, strApplicationJSON)

	/* Если  v=2  то показываем новую версию с N обьектов */
	params  := ctx.QueryArgs()
	version, _ := params.GetUint("v")

	var err error
	if version != 2  {
		if len(obj[0].WinUrl) > 0 {
			var dst []byte
			go fasthttp.Get(dst, obj[0].WinUrl)
		}
		err = json.NewEncoder(ctx).Encode(obj[0])
	} else {
		err = json.NewEncoder(ctx).Encode(obj)
	}

	if err != nil {
		ctx.Error(err.Error(), fasthttp.StatusInternalServerError)
	}
}

func FeedWorker(ctx context.Context, feed Feed, results chan<- []Response, wg *sync.WaitGroup) {

	defer wg.Done()

	response := feed.GetAd()

	select {
	case <-ctx.Done():
		return
	case results <- response:
		return
	}
}

func runFeeds(feeds ...Feed) []Response {
	results := make(chan []Response, len(feeds))
	ctx, cancel := context.WithCancel(context.Background())

	var avalibleFeeds = make([]Response, 0)
	var countUrl int
	var wg sync.WaitGroup

	for _, feed := range feeds {
		wg.Add(1)
		go FeedWorker(ctx, feed, results, &wg)
	}

	go func() {
		wg.Wait()
		close(results)
	}()

	for {
		select {
		case r := <-results:
		//	fmt.Println(r)
			if len(r) > 0 && len(r[0].Title) > 0  {
				avalibleFeeds = append(avalibleFeeds, r...)
			}
			countUrl++
			if countUrl == len(feeds) {
				return avalibleFeeds
			}
		case <-time.After(950 * time.Millisecond):
			cancel()
			return avalibleFeeds
		}
	}

	return avalibleFeeds
}

func CORS(c *routing.Context) *routing.Context {

	c.Response.Header.Set("Access-Control-Allow-Origin", "*")
	c.Response.Header.Set("Access-Control-Allow-Methods", "POST. GET, OPTIONS")
	c.Response.Header.Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Length, Accept-Encoding, Content-Type, Accept")
	c.Response.Header.Set("Content-Type", "text/html; charset=utf-8")
	c.Response.Header.Set("Cache-Control", "no-store")



	return c
}

func UserInfo(c *routing.Context) *routing.Context {

	ip := ip.GetIPAdress(c)
	c.Set("RequestId", helpers.GenUUIDv4())
	c.Set("RequestIp", ip)
	c.Set("Country", databases.GetCountry(ip))

	return c
}


func main() {

	router := routing.New()
	router.Get("/ga.php", func(c *routing.Context) error {

		c = CORS(c)
		c =  UserInfo(c)
		getAdHandler(c)
		return nil;
	})

	router.Post("/view", func(c *routing.Context) error {
		c = CORS(c)
		fmt.Fprintf(c, "Hi there! View action: RequestURI is %q", c.PostBody())
		return nil
	})

	router.Post("/click", func(c *routing.Context) error {
		c = CORS(c)
		fmt.Fprintf(c, "Hi there! Click action: RequestURI is %q", c.PostBody())
		return nil
	})

	router.Post("/cancel", func(c *routing.Context) error {
		c = CORS(c)
		fmt.Fprintf(c, "Hi there! Cancel action: RequestURI is %q", c.PostBody())
		return nil
	})

	router.Post("/empty", func(c *routing.Context) error {
		c =  UserInfo(c)
		c = CORS(c)
		fmt.Fprintf(c, "Hi there! Empty action: RequestURI is %q", c.PostBody())
		return nil
	})

	router.Options("*", func(c *routing.Context) error {
		c = CORS(c)
		return nil
	})

	s := &fasthttp.Server{
		//Handler: fasthttp.CompressHandler(router.HandleRequest),
		Handler: router.HandleRequest,
	//	LogAllErrors: true,
		Concurrency : 256 * 1024,
		// Other Server settings may be set here.
	}

	if err := s.ListenAndServe(":"+Config.Port); err != nil {
		log.Fatalf("error in fasthttp server: %s", err)
	}

}


func init() {

	var err error

	if err = configor.Load(&Config, "config/config.yml"); err != nil {
		log.Fatalf("error config load: %s", err)
	}

	if databases.ClickHouse, err = databases.ConnectClickHouseInit(Config.ClickHouse.Dsn); err != nil {
		log.Fatalf("clickhouse erros: %s", err)
	}

	HttpClient = fasthttp.Client{
		MaxConnsPerHost: 3500,
		MaxIdleConnDuration: 1 * time.Second,
	}
	
	databases.GeoDb = databases.GeoDbInit(Config.MaxMind.Path)
	models.ScopeFeedStats = models.NewFeedStatCounter(Config.Stats.FeedCounter)
}
